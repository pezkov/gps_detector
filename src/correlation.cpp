#include "correlation.h"

Correlation::Correlation(vector<complex<float>>& iData, vector<complex<float>>& gCode){
    inputData = iData;
    goldCode = gCode;
}
vector<complex<float>>& Correlation::multiply(const vector<complex<float>>& v1, const vector<complex<float>>& v2 )
{
  for (unsigned int i = 0; i < buff.size(); ++i)
  {
    buff[i]=v1[i]*v2[i];
  }
  return buff;
}

vector<complex<float>>& Correlation::conjCompVec(const vector<complex<float>>& vec){
    for (unsigned int i = 0; i < buff.size(); ++i)
    {
        buff[i]=conj(vec[i]);
    }
    return buff;
}

vector<complex<float>>& Correlation::calcCorr(){
    fftwf_execute_dft(planCode, (fftwf_complex*) &inputData[0], (fftwf_complex*) &fftSig[0]);
//    fftwf_execute_dft(planCode, (fftwf_complex*) &goldCode[0], (fftwf_complex*) &fftCode[0]);

    corrResult = multiply(fftSig,conjCompVec(goldCode));
//    for(size_t i = 0; i < corrResult.size(); ++i){
//        corrResult[i] = corrResult[i]/ corrResult.size();
//    }
    fftwf_execute_dft(planCorr, (fftwf_complex*) &corrResult[0], (fftwf_complex*) &corrResult[0]);
    for(size_t i = 0; i < corrResult.size(); ++i){
        corrResult[i] = corrResult[i]/ (corrResult.size()*corrResult.size());
    }

    return corrResult;
}

void Correlation::setParam(vector<complex<float>>& sig, vector<complex<float>>& code){
    inputData = sig;
    goldCode = code;
    fftSig.resize(sig.size());
    fftCode.resize(code.size());
    buff.resize(code.size());
}

void Correlation::setPlan(fftwf_plan &pCode, fftwf_plan& pCorr)
{
    planCode = pCode;
    planCorr = pCorr;
}
