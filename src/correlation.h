#ifndef CORRELATION_H
#define CORRELATION_H
#include <vector>
#include <cstdint>
#include <complex>
#include <algorithm>
#include "complex.h"

#include "fftw3.h"

using std::vector;


class Correlation
{
    vector<complex<float>> inputData;
    vector<complex<float>> goldCode;
    vector<complex<float>> corrResult;

    vector<complex<float>> fftSig;
    vector<complex<float>> fftCode;
    fftwf_plan planCode;
    fftwf_plan planCorr;

    vector<complex<float>> buff;
    vector<complex<float>>& multiply(const vector<complex<float>>& v1, const vector<complex<float>>& v2 );
    vector<complex<float>>& conjCompVec(const vector<complex<float>>& vec);
public:
    Correlation() = default;
    Correlation(vector<complex<float>>& iData, vector<complex<float>>& gCode);
    vector<complex<float>>& calcCorr();
    void setParam(vector<complex<float>>& sig, vector<complex<float>>& code);
    void setPlan(fftwf_plan& pCode, fftwf_plan& pCorr);

    Correlation& operator=(const Correlation& other){
        inputData = other.inputData;
        goldCode = other.goldCode;
        corrResult = other.corrResult;

        fftSig = other.fftSig;
        fftCode = other.fftCode;

        buff = other.buff;
        planCode = other.planCode;
        planCorr = other.planCorr;
        return *this;
    }
    Correlation& operator=(Correlation&& other){
        inputData = other.inputData;
        goldCode = other.goldCode;
        corrResult = other.corrResult;

        fftSig = other.fftSig;
        fftCode = other.fftCode;

        buff = other.buff;
        planCode = other.planCode;
        planCorr = other.planCorr;
        return *this;
    }
};

#endif // CORRELATION_H
