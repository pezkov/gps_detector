#ifndef COMPLEX_H
#define COMPLEX_H
#include <cmath>
template <typename T>
class complex
{
    T _re;
    T _im;
public:
    complex() = default;
    complex(T re, T im):_re(re), _im(im){}
    complex(const complex<T> &other){
        _re = other._re;
        _im = other._im;
    }
    complex(complex<T>&& other){
        _re = other._re;
        _im = other._im;
    }
    complex<T>& operator=(complex<T>&& other){
        _re = other._re;
        _im = other._im;
        return *this;
    }
    complex<T>& operator=(const complex<T>& other){
        _re = other._re;
        _im = other._im;
        return *this;
    }

    const T& real(){
        return _re;
    }
    const T& imag(){
        return _im;
    }

    void real(T re){
        _re = re;
    }

    void imag(T im){
        _im = im;
    }
    
    float fastsin(float x){
    x=x-floor(x/2/M_PI)*2*M_PI-M_PI;
    
    return (((0.00015025063885163012*x*x- 
   0.008034350857376128)*x*x+ 0.1659789684145034)*x*x-0.9995812174943602)*x;}
   
    friend complex<T> operator*(complex<T> c1, complex<T> c2){
        return complex<T>(c1.real()*c2.real() - c1.imag() * c2.imag(), c1.real()*c2.imag() + c1.imag()*c2.real());
    }
    friend complex<T> operator*(complex<T> c1, float n){
        return complex<T>(c1.real()*n, c1.imag()*n);
    }
    friend complex<T> operator/(complex<T> c1, float n){
        return complex<T>(c1.real()/n, c1.imag()/n);
    }
    friend complex<float> exp(complex<T> val){
        return complex<float>(val.fastsin(val.imag() + M_PI/2), val.fastsin(val.imag()));
    }
    friend complex<T> conj(complex<T> c){
        return complex<T>(c.real(),-c.imag());
    }

};


#endif // COMPLEX_H
