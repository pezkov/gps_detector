#ifndef CODEGEN_H
#define CODEGEN_H
#include <vector>
#include <cstdint>
#include <algorithm>
#include <string>
#include <fstream>
#include "complex.h"

using std::vector;
using std::string;
using std::ofstream;

class codeGen
{
    vector<int16_t> mSeq1;
    vector<int16_t> mSeq2;
    vector<vector<int16_t>> goldCode;
    unsigned int codeLength = 0;

    void genMsequance();
    int16_t LSFR(const vector<int16_t> polynom, vector<int16_t> &vecState);
    void upsampleVec(const vector<int16_t>& v, int n, vector<int16_t>& output);
public:
    codeGen();
    void generate();
    const vector<vector<int16_t>>& getCode();
    void getComplexCode(int prn, vector<complex<float>>& outputCode);
    void printFile(string fileName);
    void code2sig();
    void upSample(int n);
};

#endif // CODEGEN_H
