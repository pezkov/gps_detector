#include "timesamples.h"

timeSamples::timeSamples(float fSamples, float signalPeriod)
{
    Fs = fSamples;
    sigPeriod = signalPeriod;
}

void timeSamples::gerenate(){
    for(double i = 0; i < sigPeriod-(1/Fs); i+=(1/Fs)){
        tSamples.push_back(i);
    }
}

vector<float>& timeSamples::getTime(){
    return tSamples;
}
