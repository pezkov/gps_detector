#include "detector.h"


Detector::Detector(Parcer& _parcer,timeSamples& _time, FreqGen& _freq, int _nekogerTime_ms){
    parcer = _parcer;
    time = _time;
    freq = _freq;
    nekogerTime_ms = _nekogerTime_ms;
}

void Detector::initCodeGen(int Fs){
    int F0 = 1023000;
    codeGenerator.generate();
    codeGenerator.upSample(Fs/F0);
    codeGenerator.code2sig();
}

void Detector::detectSig(int Fs){
    int sigSamples = Fs/1000;
    initCodeGen(Fs);
    vector<complex<float>> signal(sigSamples*nekogerTime_ms);
    vector<complex<float>> goldCode(sigSamples);
    vector<complex<float>> buffer(sigSamples);
    planForward = fftwf_plan_dft_1d(goldCode.size(), (fftwf_complex*) &buffer[0], (fftwf_complex*) &buffer[0],
                                            FFTW_FORWARD, FFTW_MEASURE|FFTW_UNALIGNED);
    planBackward = fftwf_plan_dft_1d(goldCode.size(), (fftwf_complex*) &buffer[0], (fftwf_complex*) &buffer[0],
                                            FFTW_BACKWARD, FFTW_MEASURE|FFTW_UNALIGNED);

    for(size_t signalPart = 0; signalPart < parcer.getSignal().size() / signal.size() ; ++signalPart){
        parcer.getSignal(sigSamples*signalPart, sigSamples*(signalPart+nekogerTime_ms), signal);

        prnIter(0, maxPrn, signal, goldCode);

        result.findMax();
        result.printResult(signalPart);
        result.writeLogFile(logFile,signalPart);
    }
    fftwf_destroy_plan(planForward);
}

void Detector::prnIter(int prtStart, int prnEnd, vector<complex<float> > &signal, vector<complex<float> > &goldCode)
{
    correlator.setPlan(planForward, planBackward);
    commonCh.setCorrelator(correlator);
    vector<complex<float>> fftCode(goldCode.size());
    for(int prn = prtStart; prn < prnEnd; ++prn){
        codeGenerator.getComplexCode(prn, goldCode);
        fftwf_execute_dft(planForward, (fftwf_complex*) &goldCode[0], (fftwf_complex*) &fftCode[0]);

        commonCh.setParam(signal, time.getTime(), fftCode, nekogerTime_ms);

        fFinder.setParam(freq.getOutFreq(), commonCh);
        fFinder.calcFreqStat();
        fFinder.findMaxStat();

        result.addPrn(prn,fFinder.getDS(),fFinder.getMaxStatTotal());
    }
}

void Detector::setLogfile(std::string fName)
{
    logFile = fName;
}
