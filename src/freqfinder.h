#ifndef CHANNELMANAGER_H
#define CHANNELMANAGER_H
#include "channel.h"
#include "fftw3.h"
struct DopShift{
    int dopler;
    float shift;
};

class FreqFinder
{

    Channel Wchannel;
    vector<int> freqs;
    vector<float> doplerStat;
    vector<int> shifts;
    float maxStatTotal = 0;
    DopShift DS;
public:
    FreqFinder() = default;
    FreqFinder(vector<int> _freqs, Channel& _Wchannel);

    void calcFreqStat();
    void findMaxStat();
    DopShift getDS(){
        return DS;
    }
    const float& getMaxStatTotal(){
        return maxStatTotal;
    }
    void setParam(vector<int> _freqs, Channel& _Wchannel);
};

#endif // CHANNELMANAGER_H
