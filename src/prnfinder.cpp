#include "prnfinder.h"

prnfinder::prnfinder()
{
    DS.resize(32);
    totalStat.resize(32);
}


void prnfinder::addPrn(int prn, DopShift _DS, float _totalStat){
    DS[prn] = _DS;
    totalStat[prn] = _totalStat;
}

void prnfinder::findMax(){
//    vector<float>::iterator Max = std::max_element(totalStat.begin(), totalStat.end());
//    int index = std::distance(totalStat.begin(),Max);
//    DSresult.shift = DS[index].shift;
//    DSresult.dopler = DS[index].dopler;
//    resultPrn = index;
//    maxTotalStat = *Max;

    totalRes.resize(0);
    for(size_t i = 0; i < totalStat.size(); ++i){
        if(totalStat[i] >= threshold){
            totalRes.push_back(results(i+1,DS[i].dopler, DS[i].shift/2,totalStat[i]));
        }
    }
}

void prnfinder::printResult(int i){
    for(size_t j = 0; j < totalRes.size(); ++j){
    std::cout << "iteration " << i << " | "<< "prn " << totalRes[j].prn << ", dopler " << totalRes[j].dop
              << ", code's shift " << totalRes[j].shift
              << ", maxStat  " << totalRes[j].stat << std::endl;
    }
}

void prnfinder::writeLogFile(std::string fName, int i){
    std::ofstream out;
    out.open(fName, std::ios::app);
    if(out.is_open()){
        for(size_t j = 0; j < totalRes.size(); ++j){
        out << "iteration " << i << " | "<< "prn " << totalRes[j].prn << ", dopler " << totalRes[j].dop
                  << ", code's shift " << totalRes[j].shift
                  << ", maxStat  " << totalRes[j].stat << std::endl;
        }
    }
    out.close();

}
