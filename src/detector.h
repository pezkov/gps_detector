#ifndef DETECTOR_H
#define DETECTOR_H
#include <vector>
#include "channel.h"
#include "prnfinder.h"
#include "freqfinder.h"
#include "codegen.h"
#include "parcer.h"
#include "timesamples.h"
#include "freqgen.h"
#include "correlation.h"

#include "fftw3.h"
#include <ctime>
#include <iostream>
#include <string>
class Detector
{
    const int maxPrn = 32;
    int nekogerTime_ms;

    Parcer parcer;
    codeGen codeGenerator;
    Channel commonCh;
    Correlation correlator;
    FreqGen freq;
    timeSamples time;

    FreqFinder fFinder;
    prnfinder result;

    void initCodeGen(int Fs);

    fftwf_plan planForward;
    fftwf_plan planBackward;
    std::string logFile;
public:
    Detector() = default;;
    Detector(Parcer& _parcer, timeSamples& _time, FreqGen& _freq, int _nekogerTime_ms);
    void detectSig(int Fs);
    void prnIter(int prtStart, int prnEnd, vector<complex<float>>& signal, vector<complex<float>>& goldCode);
    void setLogfile(string fName);
};

#endif // DETECTOR_H
