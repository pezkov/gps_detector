#ifndef FREQGEN_H
#define FREQGEN_H
#include <vector>

using std::vector;
class FreqGen
{
    int fmax;
    int fmin;
    int fstep;
    vector<int> outputFreq;
public:
    FreqGen() = default;
    FreqGen(int freqmin, int freqmax, int freqstep);
    void generate();
    int getfreq(int i);
    const vector<int>& getOutFreq();
};

#endif // FREQGEN_H
