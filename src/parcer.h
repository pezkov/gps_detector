#ifndef PARCER_H
#define PARCER_H
#include <fstream>
#include <string>
#include <cstdint>
#include <vector>
#include "complex.h"

using std::vector;
using std::string;
using std::ifstream;

class Parcer
{
    string fileName;
    vector<complex<float>> Signal;
    vector<int16_t> outputData;

    void parce();
    void fillSignal();
public:
    Parcer() = default;
    Parcer(string fName);//const string&
    const vector<complex<float>>& getSignal();
    void getSignal(int samplesStart, int samplesEnd, vector<complex<float>>& outputSig);
};

#endif // PARCER_H
