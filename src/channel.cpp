#include "channel.h"


Channel::Channel(vector<complex<float>>& sig, vector<float>& t, vector<complex<float>>& gCode, int _nekogerSum)
{
    signal = sig;
    time = t;
    goldCode = gCode;
    nekogerSum = _nekogerSum;
}

Channel::Channel(int freq, vector<complex<float>>& sig, vector<float> t, vector<complex<float>>& gCode, int _nekogerSum)
{
    curfreq = freq;
    signal = sig;
    time = t;
    goldCode = gCode;
    nekogerSum = _nekogerSum;
}

complex<float> Channel::compMult(complex<float> c, float n){
    buff.imag(c.imag()*n);
    buff.real(c.real()*n);
    return buff;
}

void Channel::setCorrelator(Correlation &corr)
{
    correlator = corr;
}

void Channel::calcShiftStat(){
    complex<float> j(0,1);
//    Correlation correlator;
    std::fill(shiftStat.begin(), shiftStat.end(), 0.0);
//    std::fill(Y.begin(), Y.end(), complex<float>(0,0));
    for(size_t i = 0; i < sincos.size(); ++i){
        sincos[i] = exp(j*(time[i] * (2*M_PI*curfreq)));
    }
    // Y = signal * exp(-j(Wp_ch - Wd)*t)
    // Wp_ch - промежуточная частота, Wd - доплер
    for(int kogerSum = 0; kogerSum < nekogerSum; ++kogerSum){
        std::copy_n(signal.begin() + kogerSum*currSig.size(), currSig.size(), currSig.begin());
        for(size_t i = 0; i < currSig.size(); ++i){
            Y[i] = currSig[i] * sincos[i];
        }

        correlator.setParam(Y, goldCode);
        YC = correlator.calcCorr();

        for(size_t i = 0; i < YC.size(); ++i){
            shiftStat[i] += YC[i].real()*YC[i].real() + YC[i].imag()*YC[i].imag();
        }
    }
}

void Channel::findMaxShift(){
    vector<float>::iterator indMax = std::max_element(shiftStat.begin(), shiftStat.end());
    shift = std::distance(shiftStat.begin(),indMax);
    shiftMax = *indMax;
}

void Channel::setFreq(int _curfreq){
    curfreq = _curfreq;
}

const int& Channel::getShift(){
    return shift;
}

float Channel::getShiftMax(){
    return shiftMax;
}

void Channel::setParam(vector<complex<float>>& sig, vector<float>& t, vector<complex<float>>& gCode, int _nekogerSum){
    signal = sig;
    time = t;
    goldCode = gCode;
    nekogerSum = _nekogerSum;
    Y.resize(signal.size()/nekogerSum);
    YC.resize(signal.size()/nekogerSum);
    sincos.resize(signal.size()/nekogerSum);
    currSig.resize(signal.size()/nekogerSum);
    shiftStat.resize(signal.size()/nekogerSum);
}
