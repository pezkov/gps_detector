#ifndef CHANNEL_H
#define CHANNEL_H
#include <vector>
#include "complex.h"
#include <cmath>
#include "correlation.h"
#include <algorithm>
#include "correlation.h"

using std::vector;
class Channel
{
    int curfreq = 0;
    vector<complex<float>> signal;
    vector<float> time;
    vector<complex<float>> goldCode;
    vector<float> shiftStat;
    int nekogerSum = 0;
    int shift = 0;
    float shiftMax = 0;

    vector<complex<float>> Y;
    vector<complex<float>> YC;
    vector<complex<float>> currSig;
    vector<complex<float>> sincos;
    complex<float> buff;
    Correlation correlator;
public:
    Channel() = default;
    Channel(vector<complex<float>>& sig, vector<float>& t, vector<complex<float>>& gCode, int _nekogerSum);
    Channel(int freq, vector<complex<float>>& sig, vector<float> t, vector<complex<float>>& gCode, int _nekogerSum);


    void calcShiftStat();
    void findMaxShift();
    void setFreq(int _curfreq);
    const int& getShift();
    float getShiftMax();
    void setParam(vector<complex<float>>& sig, vector<float>& t, vector<complex<float>>& gCode, int _nekogerSum);
    complex<float> compMult(complex<float> c, float n);
    void setCorrelator(Correlation& corr);

    Channel& operator=(const Channel& other){
        curfreq = other.curfreq;
        signal = other.signal;
        time = other.time;
        goldCode = other.goldCode;
        shiftStat = other.shiftStat;
        nekogerSum = other.nekogerSum;
        shift = other.shift;
        shiftMax = other.shiftMax;
        Y = other.Y;
        YC = other.YC;
        currSig = other.currSig;
        buff = other.buff;
        correlator = other.correlator;
        sincos = other.sincos;
        return *this;
    }
    Channel& operator=(Channel&& other){
        curfreq = other.curfreq;
        signal = other.signal;
        time = other.time;
        goldCode = other.goldCode;
        shiftStat = other.shiftStat;
        nekogerSum = other.nekogerSum;
        shift = other.shift;
        shiftMax = other.shiftMax;
        Y = other.Y;
        YC = other.YC;
        currSig = other.currSig;
        buff = other.buff;
        correlator = other.correlator;
        sincos = other.sincos;
        return *this;
    }
};

#endif // CHANNEL_H
