#include "codegen.h"

codeGen::codeGen()
{
    codeLength = 1023;
    mSeq1.reserve(codeLength);
    mSeq2.reserve(codeLength);
}
void codeGen::genMsequance(){
    vector<int16_t> polynom1 = {3, 10};
    vector<int16_t> polynom2 = {2, 3, 6, 8, 9, 10};
    vector<int16_t> initState1 =  {1, 1, 1, 1, 1 ,1 ,1 ,1 ,1, 1};
    vector<int16_t> initState2 =  {1, 1, 1, 1, 1 ,1 ,1 ,1 ,1, 1};

    mSeq1.push_back(1);
    mSeq2.push_back(1);
    while(mSeq1.size() != codeLength){
        mSeq1.push_back(LSFR(polynom1,initState1));
        mSeq2.push_back(LSFR(polynom2, initState2));
    }

}
int16_t codeGen::LSFR(const vector<int16_t> polynom, vector<int16_t> &vecState){
    int16_t newBit = 0;
    for(int16_t i : polynom){
        newBit += vecState[i-1];
    }
    rotate(vecState.rbegin(), vecState.rbegin()+1, vecState.rend());
    vecState[0] = newBit%2;
    return vecState.back();
}
vector<int16_t> operator+(const vector<int16_t>& v1, const vector<int16_t>& v2 )
{
  vector<int16_t> result(v1.size());
  for (unsigned int i = 0; i < result.size(); ++i)
  {
    result[i]=v1[i]+v2[i];
  }
  return result;
}
vector<int16_t> operator%(const vector<int16_t>& v, const int base){
    vector<int16_t> newVec;
    for(int16_t i:v){
        newVec.push_back(i%base);
    }
    return newVec;
}
void codeGen::generate(){
    genMsequance();
    // сдвиги второй М-послед, соответствующие кодам 1-32(и номеру спутника)
    const vector<int16_t> chipsDelay = {5, 6, 7, 8, 17, 18, 139, 140,
                                  141, 251, 252, 254, 255, 256, 257, 258,
                                  469, 470, 471, 472, 473, 474, 509, 512,
                                  513, 514, 515, 516, 859, 860, 861, 862};
    vector<int16_t> &mSeqBuffer2 = mSeq2;
    for(auto& delay : chipsDelay){
        rotate(mSeqBuffer2.rbegin(),mSeqBuffer2.rbegin()+delay,mSeqBuffer2.rend());
        goldCode.push_back((mSeq1 + mSeqBuffer2)%2);
    }

}
const vector<vector<int16_t>>& codeGen::getCode(){
    return goldCode;
}

void codeGen::printFile(string fileName){
    ofstream outfile;
    outfile.open(fileName);
    for(int i = 0; i < 32; ++i){
        for(int j = 0; j < 1023; ++j){
            outfile << static_cast<int>(goldCode[i][j]);
        }
        outfile << std::endl;
    }
    outfile.close();
}

void codeGen::getComplexCode(int prn, vector<complex<float>>& outputCode){
    for(size_t i = 0; i < codeLength; i++){
        outputCode[i].real(goldCode[prn][i]);
    }
}

void codeGen::code2sig(){
    for(int prn = 0; prn < 32; ++prn){
        for(size_t i = 0; i < codeLength; ++i){
            goldCode[prn][i] = goldCode[prn][i]*(-2)+1;
        }
    }
}
void codeGen::upsampleVec(const vector<int16_t>& v, int n, vector<int16_t>& output){
    vector<int16_t> v0(v.size()*n);
    int k = 0;

    for(size_t i = 0; i < v0.size(); ++i){
        if(i%n == 0){
            v0[i] = v[k];
            k++;
        }else{
            v0[i] = 0;
        }
    }
    output = v0;
    for(int i = 1; i <n; ++i){
        std::rotate(v0.rbegin(), v0.rbegin() + 1, v0.rend());
        for(size_t j = 0; j< v0.size(); ++j){
            output[j] += v0[j];
        }
    }
}
void codeGen::upSample(int n){
    vector<vector<int16_t>> upsampledCode(goldCode.size(), vector<int16_t>(goldCode[0].size()*n));
    for(size_t i = 0; i < goldCode.size(); ++i){
        upsampleVec(goldCode[i],n, upsampledCode[i]);
    }
    codeLength *= 2;
    goldCode = upsampledCode;
}
