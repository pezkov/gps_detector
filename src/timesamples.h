#ifndef TIMESAMPLES_H
#define TIMESAMPLES_H
#include <vector>

using std::vector;
class timeSamples
{
    double Fs;
    double sigPeriod;
    vector<float> tSamples;
public:
    timeSamples() = default;
    timeSamples(float fSamples, float signalPeriod);
    void gerenate();
    vector<float>& getTime();
};

#endif // TIMESAMPLES_H
