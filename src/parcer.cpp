#include "parcer.h"

Parcer::Parcer(string fName)
{
    fileName = fName;
    parce();
}

void Parcer::parce(){
    ifstream file;
    file.open(fileName, std::ios::binary);

    if(file.is_open()){
        int16_t buffer;
        while(file.read((char*)&buffer,2)){
            outputData.push_back(buffer);
        }
    }
    fillSignal();
    file.close();
}

void Parcer::fillSignal(){
    for(size_t i = 0; i < outputData.size(); i+=2){
        Signal.push_back({static_cast<float>(outputData[i]),static_cast<float>(outputData[i+1])});
    }
}

const vector<complex<float>>& Parcer::getSignal(){
    return Signal;
}

void Parcer::getSignal(int samplesStart, int samplesEnd, vector<complex<float>>& outputSig){
    for(int i = 0; i < samplesEnd-samplesStart; i++){
        outputSig[i] = Signal[samplesStart + i];
    }
}
