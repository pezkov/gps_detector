#ifndef PRNFINDER_H
#define PRNFINDER_H
#include "freqfinder.h"
#include <iostream>
#include <string>
#include <fstream>
#include <vector>
using std::vector;
struct results{
    int prn;
    int dop;
    float shift;
    float stat;

    results()=default;
    results(int _prn, int _dop, float _shift, float _stat):prn(_prn), dop(_dop), shift(_shift), stat(_stat){}
};

class prnfinder
{
    const float threshold = 15;
    vector<DopShift> DS;
    int prn;
    vector<float> totalStat;

    DopShift DSresult;
    vector<results> totalRes;
    int resultPrn;
    float maxTotalStat;
public:
    prnfinder();

    void addPrn(int prn , DopShift _DS, float _totalStat );
    void findMax();
    void printResult(int i);
    void writeLogFile(std::string fName, int i);
};

#endif // PRNFINDER_H
