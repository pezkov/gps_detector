﻿#include <string>

#include "parcer.h"
#include "freqgen.h"
#include "timesamples.h"
#include "detector.h"


int main()
{
    std::string fileName = "Z:/STC/gps_final/src/testData.sig";
//    std::string logFile = "Z:/STC/gps_final/logs/detectorLog.txt";
    std::string logFile = "";
    int nekogerTime_ms = 10;
    int Fs = 2046000;

    Parcer parcer(fileName);

    timeSamples time(2046000, 0.001);
    time.gerenate();

    FreqGen freq(-10000,10000,200);
    freq.generate();

    Detector detect(parcer, time, freq, nekogerTime_ms);
    detect.setLogfile(logFile);
    detect.detectSig(Fs);



    return 0;
}
