#include "freqgen.h"


FreqGen::FreqGen(int freqmin, int freqmax, int freqstep){
    fmin = freqmin;
    fmax = freqmax;
    fstep = freqstep;
}

void FreqGen::generate(){
    for(int i = fmin; i <= fmax; i+=fstep){
        outputFreq.push_back(i);
    }
}

int FreqGen::getfreq(int i){
    return outputFreq[i];
}

const vector<int>& FreqGen::getOutFreq(){
    return outputFreq;
}
