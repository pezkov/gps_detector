#include "freqfinder.h"


FreqFinder::FreqFinder(vector<int> _freqs, Channel& _Wchannel){
    freqs = _freqs;
    Wchannel = std::move(_Wchannel);
}

void FreqFinder::calcFreqStat(){
    vector<float> zerosf(freqs.size());
    vector<int> zerosi(freqs.size());
    doplerStat = zerosf;
    shifts = zerosi;
    for(size_t i = 0;i< freqs.size();++i){
        Wchannel.setFreq(freqs[i]);
        Wchannel.calcShiftStat();
        Wchannel.findMaxShift();
        doplerStat[i] = Wchannel.getShiftMax();
        shifts[i] = Wchannel.getShift();
    }
}


void FreqFinder::findMaxStat(){
    vector<float>::iterator Max = std::max_element(doplerStat.begin(), doplerStat.end());
    int index = std::distance(doplerStat.begin(),Max);
    DS.dopler = freqs[index];
    DS.shift = shifts[index];
    maxStatTotal = *Max;
}

void FreqFinder::setParam(vector<int> _freqs, Channel& _Wchannel){
    freqs = _freqs;
    Wchannel = _Wchannel;
}
