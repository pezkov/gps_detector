1. Собрать библиотеку fftw для Linux:
	а) В директории /lib/fftw-3.3.9 выполнить mkdir build
	б) Ввести команду:
		cmake ..
	в) Выполнить команду make
2. Чтобы собрать весь проект в корне проекта выполнить: 
mkdir build
cd build
На ОС Linux запустить cmake с помощью команды:
cmake -D CMAKE_PREFIX_PATH=$PWD/.. ..
make
