#ifndef COSGENERATOR_H
#define COSGENERATOR_H
#include <vector>
#include <cmath>
#include "timesamples.h"
#include <codegen.h>
#include "complex.h"
using std::vector;
class CosGenerator
{
    int currFreq;
    int nekogerSum;
    timeSamples time;
    int prn;
    vector<complex<float>> signal;
    vector<complex<float>> goldCode;
public:
    CosGenerator() = default;
    CosGenerator(int _currFreq, timeSamples _time, int _prn, vector<complex<float>>& gCode, int _nekogerSum );

    void generate();
    vector<float> getTime(){
        return time.getTime();
    }
    vector<complex<float>>& getSignal(){
        return signal;
    }
};

#endif // COSGENERATOR_H
