#include "cosgenerator.h"



CosGenerator::CosGenerator(int _currFreq, timeSamples _time, int _prn,  vector<complex<float>>& gCode, int _nekogerSum)
{
    currFreq = _currFreq;
    time = _time;
    signal.resize(time.getTime().size());
    goldCode = gCode;
    prn = _prn;
    nekogerSum = _nekogerSum;
}

void CosGenerator::generate()
{

    for(size_t i = 0; i < time.getTime().size(); i++){
        signal[i].real(goldCode[i%goldCode.size()].real()*cos((2*M_PI*currFreq)*time.getTime()[i]));
        signal[i].imag(-goldCode[i%goldCode.size()].real()*sin((2*M_PI*currFreq)*time.getTime()[i]));
    }
}
