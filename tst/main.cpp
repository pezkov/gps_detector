#include <gtest/gtest.h>
#include "channel.h"
#include "cosgenerator.h"
#include "timesamples.h"
#include "codegen.h"
#include "freqfinder.h"
#include "freqgen.h"
#include <fftw3.h>
#include <functional>

TEST(test_min5000Hz, testfreq ){
    int curFreq = -5000;
    int prn = 0;
    int nekogerSum = 1;
    codeGen codeGenerator;
    vector<complex<float>> goldCode(2046);
    vector<complex<float>> fftBuff(2046);
    vector<complex<float>> signal;
    vector<float> timet;
    fftwf_plan planCode = fftwf_plan_dft_1d(goldCode.size(), (fftwf_complex*) &fftBuff[0], (fftwf_complex*) &fftBuff[0],
                                            FFTW_FORWARD, FFTW_MEASURE|FFTW_UNALIGNED);
    fftwf_plan planCorr = fftwf_plan_dft_1d(goldCode.size(), (fftwf_complex*) &fftBuff[0], (fftwf_complex*) &fftBuff[0],
                                            FFTW_BACKWARD, FFTW_MEASURE|FFTW_UNALIGNED);
    codeGenerator.generate();
    codeGenerator.upSample(2);
    codeGenerator.code2sig();
    codeGenerator.getComplexCode(prn, goldCode);
    fftwf_execute_dft(planCode, (fftwf_complex*) &goldCode[0], (fftwf_complex*) &goldCode[0]);

    FreqGen freq(-10000,10000,200);
    freq.generate();
    timeSamples time(2046000,0.001*nekogerSum);
    time.gerenate();
    Channel commonCh;
    Correlation correlator;
    FreqFinder fFinder;
    int probFreq;
    CosGenerator cosgen(curFreq, time, prn, goldCode, nekogerSum);
    cosgen.generate();

    signal = cosgen.getSignal();
    timet = cosgen.getTime();



    commonCh.setParam(signal, timet, goldCode, nekogerSum);
    correlator.setPlan(planCode, planCorr);
    commonCh.setCorrelator(correlator);

    fFinder.setParam(freq.getOutFreq(),commonCh);
    fFinder.calcFreqStat();
    fFinder.findMaxStat();

    probFreq = fFinder.getDS().dopler;
    fftwf_destroy_plan(planCode);
    EXPECT_EQ(probFreq, curFreq);
}
TEST(test_0Hz, testfreq ){
    int curFreq = 0;
    int prn = 0;
    int nekogerSum = 1;
    codeGen codeGenerator;
    vector<complex<float>> goldCode(2046);
    vector<complex<float>> fftBuff(2046);
    vector<complex<float>> signal;
    vector<float> timet;
    fftwf_plan planCode = fftwf_plan_dft_1d(goldCode.size(), (fftwf_complex*) &fftBuff[0], (fftwf_complex*) &fftBuff[0],
                                            FFTW_FORWARD, FFTW_MEASURE|FFTW_UNALIGNED);
    fftwf_plan planCorr = fftwf_plan_dft_1d(goldCode.size(), (fftwf_complex*) &fftBuff[0], (fftwf_complex*) &fftBuff[0],
                                            FFTW_BACKWARD, FFTW_MEASURE|FFTW_UNALIGNED);
    codeGenerator.generate();
    codeGenerator.upSample(2);
    codeGenerator.code2sig();
    codeGenerator.getComplexCode(prn, goldCode);
    fftwf_execute_dft(planCode, (fftwf_complex*) &goldCode[0], (fftwf_complex*) &goldCode[0]);

    FreqGen freq(-10000,10000,200);
    freq.generate();
    timeSamples time(2046000,0.001*nekogerSum);
    time.gerenate();
    Channel commonCh;
    Correlation correlator;
    FreqFinder fFinder;
    int probFreq;
    CosGenerator cosgen(curFreq, time, prn, goldCode, nekogerSum);
    cosgen.generate();

    signal = cosgen.getSignal();
    timet = cosgen.getTime();



    commonCh.setParam(signal, timet, goldCode, nekogerSum);
    correlator.setPlan(planCode,planCorr);
    commonCh.setCorrelator(correlator);

    fFinder.setParam(freq.getOutFreq(),commonCh);
    fFinder.calcFreqStat();
    fFinder.findMaxStat();

    probFreq = fFinder.getDS().dopler;
    fftwf_destroy_plan(planCode);
    EXPECT_EQ(probFreq, curFreq);
}
TEST(test_5000Hz, testfreq ){
    int curFreq = 5000;
    int prn = 0;
    int nekogerSum = 1;
    codeGen codeGenerator;
    vector<complex<float>> goldCode(2046);
    vector<complex<float>> fftBuff(2046);
    vector<complex<float>> signal;
    vector<float> timet;
    fftwf_plan planCode = fftwf_plan_dft_1d(goldCode.size(), (fftwf_complex*) &fftBuff[0], (fftwf_complex*) &fftBuff[0],
                                            FFTW_FORWARD, FFTW_MEASURE|FFTW_UNALIGNED);
    fftwf_plan planCorr = fftwf_plan_dft_1d(goldCode.size(), (fftwf_complex*) &fftBuff[0], (fftwf_complex*) &fftBuff[0],
                                            FFTW_BACKWARD, FFTW_MEASURE|FFTW_UNALIGNED);
    codeGenerator.generate();
    codeGenerator.upSample(2);
    codeGenerator.code2sig();
    codeGenerator.getComplexCode(prn, goldCode);
    fftwf_execute_dft(planCode, (fftwf_complex*) &goldCode[0], (fftwf_complex*) &goldCode[0]);

    FreqGen freq(-10000,10000,200);
    freq.generate();
    timeSamples time(2046000,0.001*nekogerSum);
    time.gerenate();
    Channel commonCh;
    Correlation correlator;
    FreqFinder fFinder;
    int probFreq;
    CosGenerator cosgen(curFreq, time, prn, goldCode, nekogerSum);
    cosgen.generate();

    signal = cosgen.getSignal();
    timet = cosgen.getTime();



    commonCh.setParam(signal, timet, goldCode, nekogerSum);
    correlator.setPlan(planCode,planCorr);
    commonCh.setCorrelator(correlator);

    fFinder.setParam(freq.getOutFreq(),commonCh);
    fFinder.calcFreqStat();
    fFinder.findMaxStat();

    probFreq = fFinder.getDS().dopler;
    fftwf_destroy_plan(planCode);
    EXPECT_EQ(probFreq, curFreq);
}

int main(int argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
